// Fill out your copyright notice in the Description page of Project Settings.



#include "..//Public/Players/PlayerHealthVal.h"

// Sets default values for this component's properties
UPlayerHealthVal::UPlayerHealthVal()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	DefaultHealth = 100;
	Health = DefaultHealth;
}


// Called when the game starts
void UPlayerHealthVal::BeginPlay()
{
	Super::BeginPlay();

	AActor* Owner = GetOwner();
	if(Owner)
	{ 
		Owner->OnTakeAnyDamage.AddDynamic(this, &UPlayerHealthVal::TakeDamage);
	}

	
}

void UPlayerHealthVal::TakeDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0)
	{
		return;
	}
	Health = FMath::Clamp(Health - Damage, 0.0f, DefaultHealth);

}


