// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickup.h"

#include "..//Players/PlayerMovement.h"
#include "Components/BoxComponent.h"
#include "Engine/Engine.h"

// Sets default values
APickup::APickup()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();
	TArray<UBoxComponent*> boxComps;
	GetComponents<UBoxComponent>(boxComps);

	for (int32 index = 0; index < boxComps.Num(); index++)
	{
		if (boxComps[index]->GetFName().ToString() == "Box")
		{
			BoxCollider = boxComps[index];
			BoxCollider->OnComponentBeginOverlap.AddDynamic(this, &APickup::OnOverlapBegin);
			BoxCollider->OnComponentEndOverlap.AddDynamic(this, &APickup::OnOverEndOverlap);
		}
	
	}

}

void APickup::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, 
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		if (OtherComp->ComponentHasTag("Player"))
		{
			UPlayerMovement* PlayerCollect = OtherActor->FindComponentByClass<UPlayerMovement>();

			PlayerCollect->CanCollect = true;
			PlayerCollect->TargetToCollect = this;
			PlayerCollect->AnyType = ItemType;
		}
	}
}

void APickup::OnOverEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp
	, int32 OtherBodyIndex)
{
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		if (OtherComp->ComponentHasTag("Player"))
		{
			UPlayerMovement* PlayerCollect = OtherActor->FindComponentByClass<UPlayerMovement>();

			PlayerCollect->CanCollect = false;
			PlayerCollect->TargetToCollect = nullptr;
			PlayerCollect->AnyType = "";
		}
	}
}

// Called every frame
void APickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


