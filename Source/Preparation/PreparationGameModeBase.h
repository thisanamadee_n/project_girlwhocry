// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PreparationGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PREPARATION_API APreparationGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
