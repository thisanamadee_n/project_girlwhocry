// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerInterface2.h"

// Sets default values for this component's properties
UPlayerInterface2::UPlayerInterface2()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	mainUIWidget = StaticLoadClass(UObject::StaticClass(), nullptr, TEXT("/Game/Widget/HUD_Main.HUD_Main_C"));
}


// Called when the game starts
void UPlayerInterface2::BeginPlay()
{
	Super::BeginPlay();

	GetOwner()->InputComponent->BindAction("Button_1", IE_Pressed, this, &UPlayerInterface2::Pressed_XButton);
	GetOwner()->InputComponent->BindAction("Button_2", IE_Pressed, this, &UPlayerInterface2::Pressed_AButton);
	GetOwner()->InputComponent->BindAction("Button_3", IE_Pressed, this, &UPlayerInterface2::Pressed_BButton);
	GetOwner()->InputComponent->BindAction("Button_4", IE_Pressed, this, &UPlayerInterface2::Pressed_YButton);
	
	GetOwner()->InputComponent->BindAction("PauseMenu", IE_Pressed, this, &UPlayerInterface2::Toggle_PauseMenu).bExecuteWhenPaused = true;

	if (mainUIWidget == NULL)
	{
		GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Cyan, TEXT("Cannot Find MainUI."));
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Cyan, TEXT("Found Main UI"));
	}

	if (mainUIWidget != NULL)
	{
		mainUI = CreateWidget<UMainWidget>(GetWorld(), mainUIWidget);

		if (mainUI != NULL)
		{
			mainUI->AddToViewport();

			UWidgetTree* widgetTree = mainUI->WidgetTree;

			FString widgetString = "Timer_Text";
			FName widgetName = FName(*widgetString);

			UWidget* targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL)
			{
				textTimer = Cast<UTextBlock>(targetWidget);
			}

			widgetString = "Button_1";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL)
			{
				xButtonImage = Cast<UImage>(targetWidget);
			}

			widgetString = "Button_2";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL)
			{
				aButtonImage = Cast<UImage>(targetWidget);
			}

			widgetString = "Button_3";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL)
			{
				bButtonImage = Cast<UImage>(targetWidget);
			}

			widgetString = "Button_4";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL)
			{
				yButtonImage = Cast<UImage>(targetWidget);
			}

			widgetString = "Timer_Progress";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL)
			{
				timerProgressbar = Cast<UProgressBar>(targetWidget);
			}


			widgetString = "PauseMenu_Panel";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL)
			{
				pause_Panel = Cast<UPanelWidget>(targetWidget);
			}

			widgetString = "Resume_Button";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL)
			{
				resume_Button = Cast<UButton>(targetWidget);
				resume_Button->OnClicked.AddDynamic(this, &UPlayerInterface2::Toggle_PauseMenu);

				GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Cyan, TEXT("Setup Resume Button Complete."));
			}
		}
	}
	
}


// Called every frame
void UPlayerInterface2::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (timerCounter < 0)
	{
		timerCounter = 0.0f;
	}
	
	timerCounter -= GetWorld()->GetDeltaSeconds();
	FText timerText = UKismetTextLibrary::Conv_FloatToText(timerCounter, ERoundingMode::FromZero, false, false, 1, 2, 1, 1);
	textTimer ->SetText(timerText);

	float timerRatio = timerCounter / 60.0f;
	timerProgressbar->SetPercent(timerRatio);
}

void UPlayerInterface2::Toggle_PauseMenu()
{
	if (onPause == false)
	{
		onPause = true;

		UCanvasPanelSlot* pausePanelSlot = Cast<UCanvasPanelSlot>(pause_Panel->Slot);
		pausePanelSlot->SetPosition(FVector2D(0, 0));

		UGameplayStatics::SetGamePaused(GetWorld(), true);

		ACharacter* playerCharacter = Cast<ACharacter>(GetOwner());
		APlayerController* playerController = UGameplayStatics::GetPlayerController(playerCharacter, 0);
		playerController->bShowMouseCursor = true;
		playerController->bEnableClickEvents = true;
		playerController->bEnableMouseOverEvents = true;
		playerController->SetInputMode(FInputModeGameAndUI());
	}
	else if(onPause == true)
	{
		onPause = false;

		UCanvasPanelSlot* pausePanelSlot = Cast<UCanvasPanelSlot>(pause_Panel->Slot);
		pausePanelSlot->SetPosition(FVector2D(-2000, 0));

		UGameplayStatics::SetGamePaused(GetWorld(), false);

		ACharacter* playerCharacter = Cast<ACharacter>(GetOwner());
		APlayerController* playerController = UGameplayStatics::GetPlayerController(playerCharacter, 0);
		playerController->bShowMouseCursor = false;
		playerController->bEnableClickEvents = false;
		playerController->bEnableMouseOverEvents = false;
		playerController->SetInputMode(FInputModeGameOnly());
	}
}

void UPlayerInterface2::Pressed_XButton()
{
	ActionImage_Popup(xButtonImage, 0.1f);
}

void UPlayerInterface2::Pressed_AButton()
{
	ActionImage_Popup(aButtonImage, 0.1f);
}

void UPlayerInterface2::Pressed_BButton()
{
	ActionImage_Popup(bButtonImage, 0.1f);
}

void UPlayerInterface2::Pressed_YButton()
{
	ActionImage_Popup(yButtonImage, 0.1f);
}

void UPlayerInterface2::ActionImage_Popup(UImage* targetImage, float popupTime)
{
	if (targetImage->RenderTransform.Scale == FVector2D(1.0f, 1.0f))
	{
		targetImage->SetRenderScale(FVector2D(1.5f, 1.5f));
	}

	FTimerDelegate timerDelegate = FTimerDelegate::CreateUObject(this,&UPlayerInterface2::ActionImage_Popdown, targetImage);
	FTimerHandle timer;
	GetWorld()->GetTimerManager().SetTimer(timer, timerDelegate, popupTime, false);
}

void UPlayerInterface2::ActionImage_Popdown(UImage* targetImage)
{
	targetImage->SetRenderScale(FVector2D(1.0f, 1.0f));
}