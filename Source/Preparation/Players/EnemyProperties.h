// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EnemyCharacter.h"
#include "EnemyPublicProperties.h"
#include "Engine.h"

#include "EnemyAIController.h"
#include "Runtime/NavigationSystem/Public/NavigationSystem.h"
#include "GameFramework/CharacterMovementComponent.h"


#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EnemyProperties.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PREPARATION_API UEnemyProperties : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEnemyProperties();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	ACharacter* enemyCharacter;

	AEnemyAIController* aiController;
	
	UEnemyPublicProperties* enemyPublic;
	AEnemyCharacter* enemyComp;
	int lastedCheckHP = 0;

	void Update_HealthCheck();
		
};
