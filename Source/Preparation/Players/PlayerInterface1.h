// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MainWidget.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Blueprint/WidgetTree.h"
#include "Runtime/UMG/Public/Components/Image.h"
#include "Runtime/UMG/Public/Components/Button.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "Runtime/UMG/Public/Components/ProgressBar.h"
#include "Runtime/UMG/Public/Components/EditableTextBox.h"
#include "Runtime/UMG/Public/Components/CanvasPanelSlot.h"
#include "Kismet/KismetTextLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine.h"


#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerInterface1.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PREPARATION_API UPlayerInterface1 : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerInterface1();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	TSubclassOf<class UUserWidget> mainUIWidget;

	UMainWidget* mainUI;

	UPanelWidget* pause_Panel;
	bool onPause = false;

	UTextBlock* textTimer;

	UButton* resume_Button;

	UFUNCTION()
	void Toggle_PauseMenu();
};
