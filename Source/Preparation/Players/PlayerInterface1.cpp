// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerInterface1.h"

// Sets default values for this component's properties
UPlayerInterface1::UPlayerInterface1()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	mainUIWidget = StaticLoadClass(UObject::StaticClass(), nullptr, TEXT("/Game/Widget/HUD_Main.HUD_Main_C"));
}


// Called when the game starts
void UPlayerInterface1::BeginPlay()
{
	Super::BeginPlay();

	GetOwner()->InputComponent->BindAction("PauseMenu", IE_Pressed, this, &UPlayerInterface1::Toggle_PauseMenu).bExecuteWhenPaused = true;

	if (mainUIWidget == NULL)
	{
		GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Cyan, TEXT("Cannot Find MainUI."));
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Cyan, TEXT("Found Main UI"));
	}



	if (mainUIWidget != NULL)
	{
		mainUI = CreateWidget<UMainWidget>(GetWorld(), mainUIWidget);

		if (mainUI != NULL)
		{
			mainUI->AddToViewport();

			UWidgetTree* widgetTree = mainUI->WidgetTree;

			FString widgetString = "Timer_Text";
			FName widgetName = FName(*widgetString);

			UWidget* targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL)
			{
				textTimer = Cast<UTextBlock>(targetWidget);
			}
			widgetString = "PauseMenu_Panel";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL)
			{
				pause_Panel = Cast<UPanelWidget>(targetWidget);
			}

			widgetString = "Resume_Button";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL)
			{
				resume_Button = Cast<UButton>(targetWidget);
				resume_Button->OnClicked.AddDynamic(this, &UPlayerInterface1::Toggle_PauseMenu);

				GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Cyan, TEXT("Setup Resume Button Complete."));
			}
		}

		
	}
	
}
// Called every frame
void UPlayerInterface1::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}
void UPlayerInterface1::Toggle_PauseMenu()
{
	if (onPause == false)
	{
		onPause = true;

		UCanvasPanelSlot* pausePanelSlot = Cast<UCanvasPanelSlot>(pause_Panel->Slot);
		pausePanelSlot->SetPosition(FVector2D(0, 0));

		UGameplayStatics::SetGamePaused(GetWorld(), true);

		ACharacter* playerCharacter = Cast<ACharacter>(GetOwner());
		APlayerController* playerController = UGameplayStatics::GetPlayerController(playerCharacter, 0);
		playerController->bShowMouseCursor = true;
		playerController->bEnableClickEvents = true;
		playerController->bEnableMouseOverEvents = true;
		playerController->SetInputMode(FInputModeGameAndUI());
	}
	else
	{
		onPause = false;

		UCanvasPanelSlot* pausePanelSlot = Cast<UCanvasPanelSlot>(pause_Panel->Slot);
		pausePanelSlot->SetPosition(FVector2D(-2000, 0));

		UGameplayStatics::SetGamePaused(GetWorld(), false);

		ACharacter* playerCharacter = Cast<ACharacter>(GetOwner());
		APlayerController* playerController = UGameplayStatics::GetPlayerController(playerCharacter, 0);
		playerController->bShowMouseCursor = false;
		playerController->bEnableClickEvents = false;
		playerController->bEnableMouseOverEvents = false;
		playerController->SetInputMode(FInputModeGameOnly());
	}
}
