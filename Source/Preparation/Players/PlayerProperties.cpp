// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerProperties.h"

// Sets default values for this component's properties
UPlayerProperties::UPlayerProperties()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UPlayerProperties::BeginPlay()
{
	Super::BeginPlay();

	playerCharacter = Cast<ACharacter>(GetOwner());
	meshComponent = GetOwner()->FindComponentByClass<USkeletalMeshComponent>();
	animInstance = meshComponent->GetAnimInstance();
	Light = GetOwner()->FindComponentByClass<USpotLightComponent>();

	if (animInstance != NULL)
	{
		animProperty_Speed = FindField<UFloatProperty>(animInstance->GetClass(), "Speed");
		animProperty_Active_Jump = FindField<UBoolProperty>(animInstance->GetClass(), "Active_Jump");
		animProperty_Active_Attack = FindField<UBoolProperty>(animInstance->GetClass(), "Active_Attack");
	}
}


// Called every frame
void UPlayerProperties::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	Light->SetVisibility(LightState, false);

	// ...
}

