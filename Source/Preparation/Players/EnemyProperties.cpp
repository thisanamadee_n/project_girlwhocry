// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyProperties.h"

// Sets default values for this component's properties
UEnemyProperties::UEnemyProperties()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UEnemyProperties::BeginPlay()
{
	Super::BeginPlay();

	enemyCharacter = Cast<ACharacter>(GetOwner());
	enemyComp = Cast<AEnemyCharacter>(GetOwner());
	enemyPublic = GetOwner()->FindComponentByClass<UEnemyPublicProperties>();
	//lastedCheckHP = enemyComp->enemyHealth;

	// ...
	aiController = Cast<AEnemyAIController>(enemyCharacter->GetController());

	//enemyPublic->enemyHealth = enemyComp->enemyHP;
	//enemyPublic->enemyMaxHealth = enemyComp->enemyHP;
	//lastedCheckHP = enemyComp->enemyHP;

	
}


// Called every frame
void UEnemyProperties::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	Update_HealthCheck();
	
	if (enemyPublic->onMoving == false)
	{
		enemyPublic->onMoving = true;

		UNavigationSystemV1* navigationSystem = UNavigationSystemV1::GetNavigationSystem(aiController);
		FNavLocation moveToLocationVector;
		float moveRadius = 1500.0f;

		if (navigationSystem->GetRandomPointInNavigableRadius(enemyCharacter->GetActorLocation(), moveRadius, moveToLocationVector) != NULL)
		{
			enemyCharacter->GetCharacterMovement()->MaxWalkSpeed = 100;
			aiController->MoveToLocation(moveToLocationVector, 5.0f, false);
		}
	}

	
}

void UEnemyProperties::Update_HealthCheck()
{
	if (lastedCheckHP != enemyPublic->enemyHealthPoint)
	{
		if (enemyPublic->enemyHealthPoint > 0)
		{
			//Receive Damage, Do hit Animation
		}
		else
		{
			//Die, Do die animation
			this->GetOwner()->Destroy();
		}

		lastedCheckHP = enemyPublic->enemyHealthPoint;
		GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Cyan, "Enemy_HP : " + FString::FromInt(lastedCheckHP));
	}
}
