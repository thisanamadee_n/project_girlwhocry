// Fill out your copyright notice in the Description page of Project Settings.
//Hong FlashLight Command
//Madee PlayerMovements

#pragma once
#include "PlayerProperties.h"
#include "Components/InputComponent.h"

#include "DoorDestroy.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerMovement.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PREPARATION_API UPlayerMovement : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerMovement();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPlayerProperties* playerProperties;

	void Update_AnimSpeedProperties();

	void MoveForward(float inputValue);
	void MoveRight(float inputValue);

	void TurnCamera(float inputValue);
	void PitchCamera(float inputValue);

	void PressedJump();
	void FlashLightToggle();
	void Collect();
	
	bool CanCollect = false;

	AActor* TargetToCollect;
	FString AnyType;

	ADoorDestroy* DoorDestroy;

};