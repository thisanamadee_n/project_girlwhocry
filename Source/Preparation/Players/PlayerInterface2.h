// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MainWidget.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Blueprint/WidgetTree.h"
#include "Runtime/UMG/Public/Components/Image.h"
#include "Runtime/UMG/Public/Components/Button.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "Runtime/UMG/Public/Components/ProgressBar.h"
#include "Runtime/UMG/Public/Components/EditableTextBox.h"
#include "Runtime/UMG/Public/Components/CanvasPanelSlot.h"
#include "Kismet/KismetTextLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine.h"
#include "TimerManager.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerInterface2.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PREPARATION_API UPlayerInterface2 : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UPlayerInterface2();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	TSubclassOf<class UUserWidget> mainUIWidget;

	UMainWidget* mainUI;

	float timerCounter = 60.0f;
	UTextBlock* textTimer;

	UImage* xButtonImage;
	UImage* aButtonImage;
	UImage* yButtonImage;
	UImage* bButtonImage;

	void Pressed_XButton();
	void Pressed_AButton();
	void Pressed_BButton();
	void Pressed_YButton();

	void ActionImage_Popup(UImage* targetImage, float popupTime);
	void ActionImage_Popdown(UImage* targetImage);

	UProgressBar* timerProgressbar;


	UPanelWidget* pause_Panel;
	bool onPause = false;

	UButton* resume_Button;

	UFUNCTION()
	void Toggle_PauseMenu();
};
