// Fill out your copyright notice in the Description page of Project Settings.
//Hong FlashLight Command
//Madee Player Movements


#include "PlayerMovement.h"

#include "PlayerProperties.h"
#include "Components/InputComponent.h"
#include "DoorDestroy.h"

// Sets default values for this component's properties
UPlayerMovement::UPlayerMovement()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UPlayerMovement::BeginPlay()
{
	Super::BeginPlay();

	playerProperties = GetOwner()->FindComponentByClass<UPlayerProperties>();

	GetOwner()->InputComponent->BindAxis("MoveForward", this, &UPlayerMovement::MoveForward);
	GetOwner()->InputComponent->BindAxis("MoveRight", this, &UPlayerMovement::MoveRight);
	GetOwner()->InputComponent->BindAxis("Turn", this, &UPlayerMovement::TurnCamera);
	GetOwner()->InputComponent->BindAxis("LookUp", this, &UPlayerMovement::PitchCamera);

	GetOwner()->InputComponent->BindAction("Jump", IE_Pressed, this, &UPlayerMovement::PressedJump);
	GetOwner()->InputComponent->BindAction("FlashLightToggle", IE_Pressed, this, &UPlayerMovement::FlashLightToggle);
	GetOwner()->InputComponent->BindAction("Collect", IE_Pressed, this, &UPlayerMovement::Collect);
}


// Called every frame
void UPlayerMovement::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	Update_AnimSpeedProperties();
}

void UPlayerMovement::Update_AnimSpeedProperties()
{
	if (playerProperties->animProperty_Speed != NULL)
	{
		FVector groundVelocity = playerProperties->playerCharacter->GetCharacterMovement()->Velocity;
		groundVelocity.Z = 0.0f;
		playerProperties->animProperty_Speed->SetPropertyValue_InContainer(playerProperties->animInstance, groundVelocity.Size());
	}

	if (playerProperties->animProperty_Active_Jump != NULL)
	{
		playerProperties->animProperty_Active_Jump->SetPropertyValue_InContainer(playerProperties->animInstance
			, playerProperties->playerCharacter->GetCharacterMovement()->IsFalling());
	}
}

void UPlayerMovement::MoveForward(float inputValue)
{
	FRotator currentRotation = playerProperties->playerCharacter->GetControlRotation();
	currentRotation.Pitch = 0;

	FVector forwardDirection = FRotationMatrix(currentRotation).GetScaledAxis(EAxis::X);
	playerProperties->playerCharacter->AddMovementInput(forwardDirection, inputValue);
}

void UPlayerMovement::MoveRight(float inputValue)
{
	FRotator currentRotation = playerProperties->playerCharacter->GetControlRotation();
	currentRotation.Pitch = 0;

	FVector rightDirection = FRotationMatrix(currentRotation).GetScaledAxis(EAxis::Y);
	playerProperties->playerCharacter->AddMovementInput(rightDirection, inputValue);
}

void UPlayerMovement::TurnCamera(float inputValue)
{
	playerProperties->playerCharacter->AddControllerYawInput(inputValue);
}

void UPlayerMovement::PitchCamera(float inputValue)
{
	playerProperties->playerCharacter->AddControllerPitchInput(inputValue);
}

void UPlayerMovement::PressedJump()
{
	playerProperties->playerCharacter->bPressedJump = true;
}

void UPlayerMovement::Collect()
{
	if (CanCollect == true)
	{
		GEngine->AddOnScreenDebugMessage(1, 5, FColor::White, TargetToCollect->GetName());
		
		if (AnyType == "Key")
		{
			
		}

		TargetToCollect->Destroy();
		CanCollect = false;
		TargetToCollect = nullptr;
	}
}

void UPlayerMovement::FlashLightToggle()
{
	playerProperties->LightState = !playerProperties->LightState;
	GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Cyan, playerProperties->LightState ? TEXT("TRUE") : TEXT("FALSE"));
}
