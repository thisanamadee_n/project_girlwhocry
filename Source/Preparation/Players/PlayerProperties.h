// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/GameplayStatics.h"
#include "Engine.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerProperties.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PREPARATION_API UPlayerProperties : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerProperties();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	ACharacter* playerCharacter;
	USkeletalMeshComponent* meshComponent;
	UAnimInstance* animInstance;

	UFloatProperty* animProperty_Speed;
	UBoolProperty* animProperty_Active_Jump;
	UBoolProperty* animProperty_Active_Attack;

	bool LightState = false;

	USpotLightComponent* Light;
};
