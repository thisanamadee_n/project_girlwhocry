// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"

AEnemyAIController::AEnemyAIController()
{

}

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	if (Result.IsInterrupted() == true)
	{
		enemyPublic->onMoving = false;
	}

	if (Result.IsSuccess() == true)
	{
		enemyPublic->onMoving = false;
	}
}

void AEnemyAIController::OnPossess(APawn* pawn)
{
	Super::OnPossess(pawn);
	enemyPublic = GetPawn()->FindComponentByClass<UEnemyPublicProperties>();
}
