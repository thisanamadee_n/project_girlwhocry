// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "TimerManager.h"
#include "Perception/AIPerceptionTypes.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AISenseConfig_Hearing.h"

#include "EnemyPublicProperties.h"

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class PREPARATION_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()

public:

	//Set default values for this ai controller
	AEnemyAIController();

	UEnemyPublicProperties* enemyPublic;

	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

	//Executes when controller posses a Pawn
	virtual void OnPossess(APawn* pawn) override;
	
};
