// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Preparation.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Preparation, "Preparation" );
